def sum(num1, num2):
    return num1 + num2


def quotient(num1, num2):
    return num1 / num2


try:
    # Get the user's inputs and convert them to int
    num1 = int(input("Enter the 1st number: "))
    num2 = int(input("Enter the 2nd number: "))

    # Get the sum
    #print("The sum is " + str(sum(num1, num2)))
    print(f'The sum is {sum(num1, num2)}') # interpolation, v3.6

    # Get the quotient
    print("The quotient is " + str(quotient(num1, num2)))
except:
    print('An error occurred')

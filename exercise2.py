sum = 0
for i in range(1, 10000):
    if i % 2 == 0:
        sum += i

print(f'The sum is {sum:,d}')

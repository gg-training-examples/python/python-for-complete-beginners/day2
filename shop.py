class Shop:
    __name = ''
    __products = ()

    def __init__(self, name, prod=()):
        self.__name = name
        self.__products = prod

    def add_product(self, prod):
        self.__products = self.__products + (prod,)

    def get_name(self):
        return self.__name

    def printDetails(self):
        print('Name: ' + self.__name)
        print('Products')
        
        if len(self.__products) != 0:
            for product in self.__products:
                print('- ' + product)
        else:
            print('No products')


class WineShop(Shop):
    __are_minors_allowed = False

    def __init__(self, name, are_minors_allowed=False):
        self.__name = name
        self.__are_minors_allowed = are_minors_allowed
    
    def printDetails(self):
        Shop.printDetails(self)
        print('Minors Allowed: ' + str(self.__are_minors_allowed))


def find_shop(shops, shop_name):
    for sp in shops:
        if sp.get_name() == shop_name:
            return sp


list_of_shops = [
    Shop('Red Ribbon', ('Mocha Cake', 'Black Forest')),
    Shop('I-Tech', ('PS1', 'PS2')),
    Shop('Villman'),
    Shop('Goldilocks', ('White Cake',)),
    Shop('Datablitz'),
    WineShop('A Wineshop')
]

shop1 = find_shop(list_of_shops, 'Villman')
shop1.add_product('Triple Chocolate - Half')

for shop in list_of_shops:
    shop.printDetails()
